package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/booking")
public class BookingServlet extends HttpServlet {

	private static final long serialVersionUID = 4957551653977008154L;

	public void init() throws ServletException {
		System.out.println("BookingServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Capture user input from the booking form
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		String carType = request.getParameter("car_type");
		String extrasBaby = request.getParameter("extras_baby");
		String extrasWheelchair = request.getParameter("extras_wheelchair");
		String pickupDateTime = request.getParameter("pickup_date_time");
		String pickupLocation = request.getParameter("pickup_location");
		String destination = request.getParameter("destination");
		String comments = request.getParameter("comments");
		
		HttpSession session = request.getSession();
		
		// Store all the data from the form into the session
		session.setAttribute("name", name);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("carType", carType);
		session.setAttribute("extrasBaby", extrasBaby);
		session.setAttribute("extrasWheelchair", extrasWheelchair);
		session.setAttribute("pickupDateTime", pickupDateTime);
		session.setAttribute("pickupLocation", pickupLocation);
		session.setAttribute("destination", destination);
		session.setAttribute("comments", comments);
		
		response.sendRedirect("confirmation.jsp");
	}
	
	public void destroy() {
		System.out.println("BookingServlet has been finalized.");
	}
}
