package com.zuitt;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/database")
public class DatabaseServlet extends HttpServlet {

	private static final long serialVersionUID = -6668827760365993356L;
	
	ArrayList<String> bookings = new ArrayList<>();
	private int bookingCount;

	public void init() throws ServletException {
		System.out.println("DatabaseServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String date = LocalTime.now().toString();
		
		bookingCount++;
		String orderNumber = bookingCount + date.replaceAll("[^a-zA-Z0-9]+", "");
		
		bookings.add(orderNumber);
		
		HttpSession session = request.getSession();
		session.setAttribute("bookings", bookings);
		
		response.sendRedirect("bookings.jsp");
	}
	
	public void destroy() {
		System.out.println("DatabaseServlet has been finalized.");
	}
}
