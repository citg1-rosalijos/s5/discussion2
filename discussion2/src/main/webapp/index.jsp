<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>JSP Creating Dynamic Content</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container {
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
			
			div > input, div > select, div > textarea {
				width: 97%;
			}
		</style>
	</head>
	<body>
		<div id="container">
			<h1>JSP Servlet Transport</h1>
			<form action="booking" method="post">
				<div>
					<label for="name">Name: </label>
					<input type="text" name="name" required>
				</div>
				
				<div>
					<label for="phone">Phone Number: </label>
					<input type="tel" name="phone" required>
				</div>
				
				<div>
					<label for="email">Email Address: </label>
					<input type="email" name="email" required>
				</div>
				
				<fieldset>
					<legend>Which vehicle do you require?</legend>
					
					<input type="radio" id="taxi" name="car_type" value="taxi" required>
					<label for="taxi">Taxi</label>
					
					<input type="radio" id="four_seater" name="car_type" value="fourseater" required>
					<label for="four_seater">4-Seater</label>
					
					<input type="radio" id="six_seater" name="car_type" value="sixseater" required>
					<label for="six_seater">6-Seater</label>
				</fieldset>
				
				<fieldset>
					<legend>Extras</legend>
					
					<input type="checkbox" name="extras_baby" value="baby_seat">
					<label for="extras_baby">Baby Seat</label>
					
					<input type="checkbox" name="extras_wheelchair" value="wheelchair_assistance">
					<label for="extras_wheelchair">Wheel Chair Assistance</label>
				</fieldset>
				
				<div>
					<label for="pickup_date_time">Pick-up Date and Time</label>
					<input type="datetime-local" name="pickup_date_time" required>
				</div>
				
				<div>
					<label for="pickup_location">Pickup Location</label>
					<select id="pickup_place" name="pickup_location" required>
						<option value="" selected>Select one</option>
						<option value="office">Office</option>
						<option value="home">Home</option>
					</select>
				</div>
				
				<div>
					<label for="destination">Drop off Place</label>
					<input type="text" name="destination" required list="destinations">
					
					<!-- datalist provide autocomplet feature -->
					<datalist id="destinations">
						<option value="Home">
						<option value="Office">
						<option value="Airport">
						<option value="Beach">
					</datalist>
				</div>
				
				<div>
					<label for="comments">Special Instructions</label>
					<textarea name="comments" maxlength="500"></textarea>
				</div>
				
				<button>Submit booking</button>
			</form>
		</div>
	</body>
</html>